﻿namespace Pepper.Models
{
    internal class Account
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public decimal CurrentAmount { get; set; }

        public AccountType Type { get; set; }
    }
}
