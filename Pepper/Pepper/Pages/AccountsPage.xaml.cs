﻿using Pepper.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Pepper.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AccountsPage : ContentPage
    {
        public AccountsPage()
        {
            InitializeComponent();

            accountsListView.ItemsSource = new List<Account>()
            {
                new Account(){Name = "Cash/Wallet", CurrentAmount = 100.50M, Type = new AccountType(){ Name = "Income"} },
                new Account(){Name = "MMBL", CurrentAmount = 100.50M, Type = new AccountType(){ Name = "Income"} },
                new Account(){Name = "CBL", CurrentAmount = 100.50M, Type = new AccountType(){ Name = "Expense"} },
                new Account(){Name = "ABBL", CurrentAmount = 100.50M, Type = new AccountType(){ Name = "Expense"} },
            };
        }

        private void AccountsListView_Refreshing(object sender, EventArgs e)
        {
            Task.Delay(2000);
            accountsListView.EndRefresh();
        }
    }
}