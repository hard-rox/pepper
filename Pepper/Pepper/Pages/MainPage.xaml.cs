﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Pepper.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : TabbedPage
    {
        public MainPage()
        {
            InitializeComponent();
        }
    }
}